OpenStack client
================

Docker container image with the latest version of OpenStack and Heat python's clients.


Registries
----------

### Gitlab

Automated builds are available at [GitLab](https://gitlab.com/maro4inter/openstack-client)

  * Alpine:  
    `docker pull registry.gitlab.com/maro4inter/openstack-client:alpine`
  * CentOS:  
    `docker pull registry.gitlab.com/maro4inter/openstack-client:centos`
  * Fedora:  
    `docker pull registry.gitlab.com/maro4inter/openstack-client:fedora`
  * Ubuntu:  
    `docker pull registry.gitlab.com/maro4inter/openstack-client:ubuntu`


### Docker hub

Alternative [automated build](https://hub.docker.com/r/mar4eva/openstack-client/)
are available from the Docker registry.

  * Alpine:  
    `docker pull mar4eva/openstack-client:alpine`
  * CentOS:  
    `docker pull mar4eva/openstack-client:centos`
  * Fedora:  
    `docker pull mar4eva/openstack-client:fedora`
  * Ubuntu:  
    `docker pull mar4eva/openstack-client:ubuntu`

Usage
-----

### Cloud configurations
Place your `clouds.yaml` in `~/.config/openstack` More information about
`clouds.yaml` can be found [here](http://docs.openstack.org/developer/python-openstackclient/configuration.html)

```
$ cat ~/.config/openstack/clouds.yaml
```

    clouds:
      openwatt:
        auth:
          auth_url: https://openwatt.com:5000/v2.0
          project_name: dhc123456
          username: c4pash3n1
          password: 0p3nSt4ck
        region_name: RegionOne
      oiaas:
        auth:
          auth_url: http://x.x.x.x:5000/v2.0
          project_name: facebook89115xxxx
          username: facebook89115xxxx
          password: xxxxxxxxxxxxxxxxx
        region_name: RegionOne


### Issue command

Perform a `nova list` or `openstack server list` for Openwatt using `clouds.yaml`:
```
$ openstack --os-cloud openwatt server list
```

Thanks
------

  * Lars Kellogg-Stedman, for releasing [OpenStack Tools](https://github.com/larsks/openstack-tools)
  * Gerard Braad, from releasing [OpenStack Client](https://github.com/gbraad/dockerfile-openstack-client)
  * [GitLab](https://gitlab.com/) for providing a great infrastructure