#!/bin/sh

# Install dependencies
apk add --update --no-cache python py-pip git python-dev libffi libffi-dev \
    openssl openssl-dev build-base iputils bash curl linux-headers
pip install --upgrade pip

# Install Openstack client
git clone https://github.com/openstack/python-openstackclient.git /root/openstackclient --depth 1
cd /root/openstackclient
pip install --no-cache-dir .

# Install Openstack Heat client
git clone https://github.com/openstack/python-heatclient.git /root/heatclient --depth 1
cd /root/heatclient
pip install --no-cache-dir .

# Cleanup
apk del build-base linux-headers python-dev libffi-dev openssl-dev
rm -rf /var/cache
